
import ROOT
ROOT.gROOT.SetBatch(True)
import csv

## "COVID-19 Total Fatalities by County, 3/7/2020 - 10/03/2020 at 6:30 PM CST" ##
# 3 data blocks seperated by county name
# block 1 is x axis, 2 is dallas y axis , 3 is tarrant y axis, and 4 is total y-axis


outF = ROOT.TFile.Open("covid_histo.root", "RECREATE")

death_graph = ROOT.TH1F("d","COVID Death Test",1000,0.,50.)

with open('/cluster/home/quentiny/COVID_Histo/Deaths_by_TX_County.csv','r') as csv_deaths:
	deaths = csv.reader(csv_deaths)

	for line in csv_deaths:
		print(line)
		for i in xrange(len(line)):
			if i == ",":
				continue
			else:
				entry = float(line[i])
				death_graph.Fill(entry)
			#death_graph.Fill(line[i])


canvas = ROOT.TCanvas()
death_graph.Draw("HIST")
death_graph.Print("death_graph_hist.pdf")

outF.Write()
outF.Close()

#!/usr/bin/env python2.7

'''

Description:
--------------
A script that fills a histogram with 100 entries and checks how the error are calculated before rebinning.

python scripts/checking_bin_errors.py

Sanity check the error setting and the Bin entries that are in the histogs for the fit.

	create a flat histo with 100 in each bin, original is 100 rebin to have 10 bins.
	Check the entries and the errors before and after rebin 
	scale the bins by the width and then check the entries and the errors.

'''


# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )


#### MUST SETUP AthDerivation_21.2.6.0 For this to work ####


def scale_width(hist,weights=False):


  for bin in xrange(hist.GetNbinsX()+2): # Loops over all bins including over and underflow, even though there should be no entries there
    width = hist.GetBinWidth(bin);
    if width == 0: 
      continue
    hist.SetBinContent( bin, hist.GetBinContent(bin) / width ) # Try this without scaling by the width
    if weights:
      hist.SetBinError( bin, hist.GetBinError(bin) / width ) # Try this without scaling by the width, comment this and run again?
  return hist


import array

flat_hist = ROOT.TH1F("Flat Histo", "Original Histo", 100, 0, 100)

new_bin_array = [0.0,10.0,20.0,30.0,40.0,50.0,70.0,80.0,90.0,100.0]
# Necessary to have bin array be of this type
inclusiveBinning = array.array('d', new_bin_array)


# Bin error should be 1/sqrt(N) ? What Root website says the errors are are just sqrt(N)
bin_error_original = []
bin_error_rebinned = []
bin_error_scaled_rebinned = []
bin_error_scaled_rebinned_sumw2 = []






for entry in xrange(100):
  for bin in xrange(100):
      flat_hist.Fill(bin)

for bin in xrange(99):
  bin_error_original.append(flat_hist.GetBinError(bin))

print("Bin error for Original:") # we expext 1/sqrt(100) = 1/10 = .1
print(bin_error_original)






canvas = ROOT.TCanvas()
flat_hist.Draw("HIST")
canvas.Print("flathist-plot.pdf")


#* -----------------------
#* Starting rebinning
#* -----------------------
rebinned_hist = flat_hist.Rebin( 8, "Rebinned Hist" , inclusiveBinning)
for bin in xrange(len(new_bin_array)-1):
  bin_error_rebinned.append(rebinned_hist.GetBinError(bin))

print("Bin error for rebin:")
print(bin_error_rebinned)

canvas = ROOT.TCanvas()
rebinned_hist.Draw("HIST")
canvas.Print("rebinnedhist-plot.pdf")



#* starting code for bin width scaled hist
scaled_rebinned_hist = scale_width(rebinned_hist)
for bin in xrange(len(new_bin_array)-1):
  bin_error_scaled_rebinned.append(scaled_rebinned_hist.GetBinError(bin))


print("Bin error for scaled rebin:")
print(bin_error_scaled_rebinned)



print("Bin error for scaled rebin with Sumw2():")

scaled_rebinned_hist.Sumw2()
for bin in xrange(len(new_bin_array)-1):
  bin_error_scaled_rebinned_sumw2.append(scaled_rebinned_hist.GetBinError(bin))

print(bin_error_scaled_rebinned_sumw2)



canvas = ROOT.TCanvas()
scaled_rebinned_hist.Draw("HIST")
canvas.Print("scaledrebinnedhist-plot.pdf")



print("Finished and done and finished and done")


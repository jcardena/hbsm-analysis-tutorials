#!/usr/bin/env python2.7

# Set up ROOT and RootCore:
import ROOT
import argparse

## add arg parse to create ntuples from any files fed in.

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

testfile="/cluster/home/bburghgr/truth-hstaustau/daod/DAOD_TRUTH3.test.pool.root"
amys_file="/cluster/home/amyrewoldt/DAOD/3lep_Hplus500_slep110_Xn165_Xc120/truth1/DAOD_TRUTH1.aod.pool.root"

fileName = amys_file

f = ROOT.TFile.Open(fileName, "READONLY")
tree = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")

print "Number of input events:", tree.GetEntries()

outF = ROOT.TFile.Open("3lep_ntuple.root", "RECREATE")
outTree = ROOT.TTree("CollectionTree", "Test TTree")

# Create vectors to store tlorentzvectors
# ROOT.vector(ROOT.TLorentzVector)() == std::vector<TLorentzVector>()
taus = ROOT.vector(ROOT.TLorentzVector)()
elecs = ROOT.vector(ROOT.TLorentzVector)()
muons = ROOT.vector(ROOT.TLorentzVector)()
jets = ROOT.vector(ROOT.TLorentzVector)()
mets = ROOT.vector(ROOT.TLorentzVector)()

# Create branches
outTree.Branch("taus", taus)
outTree.Branch("elecs", elecs)
outTree.Branch("muons", muons)
outTree.Branch("jets", jets)
outTree.Branch("mets", mets)



for entry in xrange(tree.GetEntries()):
  tree.GetEntry(entry)

  inElecs = tree.TruthElectrons
  inMuons = tree.TruthMuons
  inTaus = tree.TruthTaus
  inJets = tree.AntiKt4TruthDressedWZJets
  inMets = tree.MET_Truth
  elecs.clear()    # removes previous vector information to preveious entry in the loop
  muons.clear()
  taus.clear()
  jets.clear()
  mets.clear()
  # Do cuts and overlap removal here
  for elec in inElecs: elecs.push_back(elec.p4())
  for mu in inElecs: muons.push_back(mu.p4())
  for tau in inTaus: taus.push_back(tau.p4())
  for jet in inJets: jets.push_back(jet.p4())
  for met in inMets:
    mp4 = ROOT.TLorentzVector()
    mp4.SetPtEtaPhiM(met.met(), 0, met.phi(), 0)
    mets.push_back(mp4)
  outTree.Fill()
  print "DEBUG: Event", entry, "taus:", len(taus), "jets:", len(jets), "mets:", len(mets)

outF.Write()
outF.Close()
print "Finished"

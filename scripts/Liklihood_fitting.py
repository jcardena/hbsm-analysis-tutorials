#! /usr/bin/ python


# import numpy as np
import math
import random
import matplotlib.pyplot as plt

#Creating a sample distribution to fit


mu_h0 = float(2)
mu_h1 = float(5)
h0_bins = [] # Null hypothesis, bkg only
h1_bins = [] # Alternative hypothesis, sig + bkg

def poisson_func(n,mu):
    return ( (mu**n)/ math.factorial(n) ) * math.exp(-mu)


# creating bins for Poisson
bin_number = 10
for i in range(100):
    # print(i)
    rand_num = random.randint(0,10)
    bin_values_h0 = poisson_func(rand_num,mu_h0)
    bin_values_h1 = poisson_func(rand_num,mu_h1)

    # bin_values_h0 = round(bin_values_h0,3)
    # bin_values_h1 = round(bin_values_h1,3)
    
    h0_bins.append(bin_values_h0)
    h1_bins.append(bin_values_h1)

print(h0_bins) 
print(h1_bins)

liklihood_values = []

test_mu = 50
liklihood = 1
for sample_mu in range(1,10):
    liklihood = 1
    for i in h0_bins:
        liklihood*= poisson_func(i,float(sample_mu))
    
    liklihood_values.append(liklihood)

print(liklihood_values)





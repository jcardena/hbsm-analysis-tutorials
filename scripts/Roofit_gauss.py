
import ROOT

# Construct a Gaussian probability density model
		
#RooRealVar creates a variable allowed to float in the given range.
# RooRealVar::RooRealVar	(	const char * name, const char * title, Double_t minValue, Double_t maxValue, const char * unit = ""  )
x = ROOT.RooRealVar("x","x",0,-10,10) 
mean = ROOT.RooRealVar("mean","Mean of Gaussian",0,-10,10)
width = ROOT.RooRealVar("width","With of Gaussian",3,0.1,10)
g = ROOT.RooGaussian("g","Gaussian", x , mean, width)


# Construct a Poisson probability model 
n = ROOT.RooRealVar("n","Observed event cont",0,0,100)
mu = ROOT.RooRealVar("mu","Expected event count",10,0,100) 
p = ROOT.RooPoisson("p","Poisson",n, mu)

# generate unbnined dataset of 10k events 
toyData = g.generate(ROOT.RooArgSet(x), 10000)
# Perform unbinned ML fit to toy data 
g.fitTo(toyData)
# # Plot toy data and pdf in observable x 
frame = x.frame() 
toyData.plotOn(frame) 
g.plotOn(frame) 
frame.Draw()




# Create Likelihood function L(x|mu, sigma) for all x in toy data. nll is negative log liklihood function
nll = g.createNLL(toyData)
nll.Print() # RooNLLVar::nll_g_gData[ paramSet=(mean,width) ] = 25055.6

# ML estimation of model parameters mean, width
m = ROOT.RooMinimizer(nll) 
m.migrad() # Minimization m.hesse() # Hessian error analysis

# Result of minimisation and error analysis is propagated to variable objects representing model parameters 
mean.Print() 
width.Print()

# Visualize likelihood L(mu, sigma) # at sigma = sigma_hat in range 2.9<mu<3.1
frame2 = mean.frame(2.9,3.1) 
nll.plotOn(frame2) 
frame2.Draw()

c = ROOT.TCanvas("rf101_basics", "rf101_basics", 800, 400)
c.Divide(2)
c.cd(1)
ROOT.gPad.SetLeftMargin(0.15)
frame.GetYaxis().SetTitleOffset(1.6)
frame.Draw()
c.cd(2)
ROOT.gPad.SetLeftMargin(0.15)
frame2.GetYaxis().SetTitleOffset(1.6)
frame2.Draw()
 
c.SaveAs("gaussian_basics.png")
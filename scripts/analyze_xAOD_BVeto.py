#!/usr/bin/env python                                                                                         


'''
Purpose:
-----------
The purpose of this script is to investigate how to access the data in a MET_Reference Container
for the purposes of using it to make a cut in the MET in the derivation framework code



Prereq:
---------
In order for this script to run, you must setup an athena environment like AthDerivationX.Y
so that root files and their contents, xAOD or otherwise become accessible to you


How to Run this Script:
--------------------------
python analyze_xAOD.py /path/to/root/file/file.root -frac .01


/path/to/root/file/file.root
  This is the root file that you want to run this script on, must be the full path to the scrtpt
frac
  A number between 0 and 1 that will tell the program what percentage of the total events you want to process



'''




import argparse
import inspect
# import matplotlib.pyplot as plt
# import numpy as np

print("Make sure to have setup an Athena Version")


def histograms():
  import ROOT
  JetScore = ROOT.TH1F("RNNJetScore", "RNNJetScore", 50, 0, 1)
  SigTrans = ROOT.TH1F("RNNJetScore SigTrans","RNNJetScore SigTrans", 50, 0,1)
  
  pt = ROOT.TH1F("pt taus", "pt taus", 100, 15e3, 2e6)
  low_pt = ROOT.TH1F("low pt taus", "low pt taus", 20, 15e3, 100e3)
  
  pt_above_95_flattened = ROOT.TH1F("pt taus when SigTrans>.95", "pt taus when SigTrans>.95", 100, 15e3, 2e6)
  prong_above_95_flattened = ROOT.TH1F("prong # when SigTrans>.95","prong # when SigTrans>.95",5,0,5)
  RNN_score_above_95_flattened = ROOT.TH1F("RNN score when SigTrans>.95","RNN score when SigTrans>.95",100, .95,1)
  
  pt_below_04_flattened = ROOT.TH1F("pt taus when SigTrans<.04", "pt taus when SigTrans<.04", 100, 15e3, 100e3)
  prong_below_04_flattened = ROOT.TH1F("prong # when SigTrans<.04","prong # when SigTrans<.04",5,0,5)
  RNN_score_below_04_flattened = ROOT.TH1F("RNN score when SigTrans<.04","RNN score when SigTrans<.04",50, 0,1)
  
  prong_for_RNNScore_below_0 = ROOT.TH1F("prong # when RNNScore < 0","prong # when RNNScore < 0",5,0,5)

  BTagScore = ROOT.TH1F("DL1rnn_pb","DL1rnn_pb", 50, -4,4)


def draw_hists( outF, hist_array=[]):
  import ROOT
  canvas = ROOT.TCanvas()
  for hist in hist_array:
    hist.Draw("HIST")
    print(hist.fName())
    canvas.Print("tau_pt_hist.pdf")



  outF.Write()
  outF.Close()



def main(args):

  # Set up ROOT and RootCore:                                                                                      
  import ROOT
  ROOT.gROOT.SetBatch(True)
  #ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )                                                      
  # Initialize the xAOD infrastructure:
  if(not ROOT.xAOD.Init().isSuccess()):
    print("Failed xAOD.Init()")
  
  # fileName=args.xAOD

  # Data currently being analyzed is here
  #MULTI_JET_MC16_DATA=/eos/home-j/jcardena/HBSM_analysis/control_region_jets_task/xAODs/MC_xAODs/MultiJet/mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.AOD.e3569_s3126_r10201_r10210/
  # WJET_MC16_DATA="/eos/home-j/jcardena/HBSM_analysis/fake_factors_task/data/xAODs/MC_xAODs/WJets/mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.merge.AOD.e5340_s3126_r10201_r10210/AOD.13070496._000006.pool.root.1"
  DATA_HIGGS6D = "/eos/home-j/jcardena/HBSM_analysis/fake_factors_task/data/DATA/data18_13TeV.00349533.physics_Main.merge.AOD.f937_m1972/data18_13TeV.00349533.physics_Main.merge.AOD.f937_m1972._lb0118._0001.1"
  OS_LEP = "/eos/home-j/jcardena/HBSM_analysis/test_dir/DAOD_SR_TAULEP.SR_TAULEP_Hpana_DxAOD_DATA.pool.root"
  fileName = DATA_HIGGS6D
  outF = ROOT.TFile.Open("sample_histo.root", "RECREATE")
  print("Analysing file: {}".format(fileName))
  # Calling all histograms in above function, to make main() more tidy
  histograms()


  
  f = ROOT.TFile.Open(fileName, "READONLY")
  t = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")
  
  print("Number of input events:", t.GetEntries())
  
  
  

  # Determining what fraction of the total events to process
  if args.fraction != 1:
    nevents=int(args.fraction*t.GetEntries()) # Making sure fraction is an integer
    print("Processing {} events\n".format(nevents))
  else:
    nevents = t.GetEntries()
    print("Processing all events: {}".format(t.GetEntries()))


  quark_jets = 0
  gluon_jets = 0

  # Actual data analysis takes place below
  tau_jets_are_pt_sorted=0
  events_w_0_bjets = 0
  events_w_1_bjets = 0
  events_w_2_or_more_bjets = 0

  SV1_bjets = 0
  IP2D_bjets = 0
  IP3D_bjets = 0
  MVA_bjets = 0
  DL1_bjets = 0

  BTagScore = ROOT.TH1F("DL1rnn_pb","DL1rnn_pb", 50, -4,4)
  for entry in range(nevents): # Loop through each proton proton bunch crossing "event", stored in the data
  # for entry in range(1):
    print("Entry: {}".format(entry))
    t.GetEntry(entry)         # Loading entry in we are currently on in the loop


    # Initialzing containers to be accessed
    # ------------------------------------------
    # You can view all accessible containers by setting up a release and then doing
    # checkxAOD.py on the .root file that you want to check the containers on.
    jets_container = t.AntiKt4EMPFlowJets
    # truth_event_container = t.TruthEvents # Tutorial said that I should access truth particles via Truth Event links or I'll get the pileup particles as well
    tau_jets_container = t.TauJets        # care about the seed jet of the taus, maybe save a flag to the tau that tells it what created it
    # truth_particle_container = t.TruthParticles


    met_ref = t.MET_Reference_AntiKt4EMPFlow # has objects of type xAOD::MissingET_v1,           but is a xAOD::MissingETContainer_v1 container
    met_assoc = t.METAssoc_AntiKt4EMPFlow    # has objects of type xAOD::MissingETAssociation_v1 but is a xAOD::MissingETAssociationMap_v1
    met_core = t.MET_Core_AntiKt4EMPFlow     # has objects of type xAOD::MissingET_v1            but is a xAOD::MissingETContainer_v1
    btag_EM_Topo = t.BTagging_AntiKt4EMTopo # has objects of type xAOD::BTagging_v1              but is a ROOT.DataVector<xAOD::BTagging_v1> object
    electrons_container = t.Electrons
    primary_verticies = t.PrimaryVertices


    # print("Number of primary Verticies: {}".format(len(primary_verticies)))
    
    bjets_SV1 = 0
    # print("Container Name: BTagging_AntiKt4EMTopo")
    # print(btag_EM_Topo)
    # for i in btag_EM_Topo:

      # if i.SV1_pb() >= .7:
      #   SV1_bjets += 1
      #   # print("SV1_pb:",i.SV1_pb())
      # if i.IP2D_pb() >= .7:
      #   IP2D_bjets += 1
      #   print("IP2D_pb:",i.IP2D_pb())
      # if i.IP3D_pb() >= .7:
      #   IP3D_bjets += 1
      #   print("IP3D_pb:",i.IP3D_pb())
      # if i.JetFitter_pb() >= .7:
      #   jetfitter_bjets += 1
      #   print("JetFitter_pb", i.JetFitter_pb())
      # if i.MV1_discriminant() >= .7:
      #   MVA_bjets += 1
      #   print("MVA:",i.MV1_discriminant())
      # if i.auxdata("DL1rnn_pb") >= .7:
      #   DL1_bjets +=1
      #   print(i.auxdata("DL1rnn_pb"))


    b_jets = 0
    tag_list = sorted(btag_EM_Topo, key=lambda x: x.pt())
    for bjet in btag_EM_Topo:
      BTagScore.Fill(bjet.auxdata("DL1rnn_pb"))
      print(bjet.pt()) # BTagging Container dosnst have a pt attribute, might have to get the jet link to access it. and pt sort
      # if i.auxdata("DL1rnn_pb") > .70:
        # b_jets += 1
        # print(i.auxdata("DL1rnn_pb"))
      # print(i.auxdata("DL1rnn_pb")) # Works
      # value=3
      # print(i.pb("DL1rnn_pb"))
    # print(b_jets)
    # if b_jets > 0:
      # print("Entry: {}, has {} BJets".format(entry, b_jets))
    

    # if b_jets == 0:
    #   events_w_0_bjets += 1
    # elif b_jets == 1:
    #   events_w_1_bjets += 1
    # elif b_jets >1:
    #   events_w_2_or_more_bjets += 1
  
    
      

    # if len(tau_jets_container) == 0:
    #   continue


    # print("Container Name: TauJets")
    # print("No. Jets: {}".format(len(tau_jets_container)))
    # pt_array = []
    # print("Len of Electrons Container: ", len(electrons_container))
    # for i in tau_jets_container:
    #   print(i.charge()*electrons_container[0].charge())
      # pt_array.append(i.pt())
      # print(i.pt())

    # # print(pt_array)
    # max_pt = max(pt_array)
    # print(max_pt)
    

    # if max_pt == tau_jets_container[0].pt():
    #   tau_jets_are_pt_sorted+=1

    # print("\n")

    # print("Container Name: Electrons")
    # for i in electrons_container:
      # print(i.charge())
      # print(i.Tight())
      # print(i.auxdata("Tight"))
      # print(i.isAvailable("Tight"))

    #Try this for BVeto Veto: Tight
   
    # print("Container Name: MET_Reference_AntiKt4EMPFlow")
    # # print(met_ref["MissingET"].met()) # Did not work
    # for i in met_ref:
    #   print(i.name())
    #   print(i.met())
    #   print(i.sumet())
    # print("\n")
    # my_list = inspect.getmembers(met_ref["FinalClus"])
    # print(my_list)

    # print("Container Name: METAssoc_AntiKt4EMPFlow")
    # # print(met_assoc["MissingET"])
    # for i in met_assoc:
    #   print(i)
    #   # print(i.name())
    # print("\n")

    
    # print("Container Name: MET_Core_AntiKt4EMPFlow")
    # for i in met_core:
    #   print(i.name())
    #   print(i.met())
    #   print(i.sumet())
    # print("\n")


    # print(met_container["FinalClus"].sumet())

    # met_total = 0
    # for i in met_container:
    #   print(i.name())
    #   print(i.met())
    #   print(i.sumet())
    #   met_total += i.sumet()
    # met_total = met_total/1000
    # print("total MET:", met_total)
    

  # https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf # PdgIDs of particles
  # https://gitlab.cern.ch/atlas/athena/-/blob/master/Event/xAOD/xAODJet/xAODJet/versions/Jet_v1.h # CAllable functions on AntiKt4EMPFlowJets or anything Jet_v1
  # https://gitlab.cern.ch/atlas/athena/-/blob/master/Event/xAOD/xAODJet/xAODJet/versions/JetAccessorMap_v1.h # Can call getattribute for these
  # https://gitlab.cern.ch/atlas/athena/-/blob/master/Event/xAOD/xAODTruth/xAODTruth/versions/TruthParticle_v1.h # functions callable for Truth particles
    # for index,jet in enumerate(jets_container):
    #   print("Jet {} in event {} has pt {}".format(index,entry,jet.pt()))
    #   # print(jet.PartonTruthLabelID())
    #   Initiation_particle=jet.getAttribute("PartonTruthLabelID")
    #   if abs(Initiation_particle) <=6 :
    #     quark_jets+=1
    #   if Initiation_particle == 21:
    #     gluon_jets+=1 
#        print(Initiation_particle)

 #   for true_particle in truth_particle_container:
 #     print("Number of Children for paricle with pdgID {}: {}".format(true_particle.pdgId(), true_particle.nChildren()) )



  # print(args.xAOD)
  # print("# of Quark Initiated Jets: {}".format(quark_jets))
  # print("# of Gluon Initiated Jets: {}".format(gluon_jets))
  print("Num events:",nevents)

  # print("SV1_bjets: {}".format(SV1_bjets))
  # print("IP2D_bjets: {}".format(IP2D_bjets))
  # print("IP3D_bjets: {}".format(IP3D_bjets))
  # print("MVA_bjets: {}".format(MVA_bjets))
  # print("DL1_bjets: {}".format(DL1_bjets))

  # print("Number of pt sorted TauJet containers:", tau_jets_are_pt_sorted)
  # print("Events with 0 bjets: {}".format(events_w_0_bjets))
  # print("Events with 1 bjets: {}".format(events_w_1_bjets))
  # print("Events with >=2 bjets: {}".format(events_w_2_or_more_bjets))

  draw_hists(outF,[BTagScore])



 

# Begin running the main function
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description='Processing a ROOT xAOD')
  parser.add_argument('xAOD', type=str, help='The path to the .root file you want to analyze')
  parser.add_argument('-frac', '--fraction', type=float, default=1, help='The number of events you want to process')
  # parser.add_argument('--sum', dest='accumulate', action='store_const',
  #                     const=sum, default=max,
  #                     help='sum the integers (default: find the max)')
  
  args = parser.parse_args()
  

  main(args)


  

#EOF 

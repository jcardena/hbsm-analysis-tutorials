#!/usr/bin/env python                                                                                         


'''
Purpose:
-----------
The overall purpose of this script is to Analyze a root file (xAOD or DAOD) and truth match jets.
Which is basically trying to find what particle initiated a given jet.

In MonteCarlo data (MC) proton proton colloisions are simulated and allowed to decay as they would to our best understanding in
reality, these simulated particles then collide with the walls of a simulated ATLAS detector. This Simulated ATLAS detector 
sees these collosions as simple floating point values. Another software takes in these collision values to try and "Reconstruct" the particle characteristics,
like position (eta,phi), and Tansverse Energy (Et) or Transverse Momentum (Pt) it also tries to  reconstruct what simulated particle in 
actuality collided with the simulated ATLAS detector. So a particle could collide with the detector and the software
could determine that the particle that collided was an electron with certian characteristics. 
We say that this particle was then "Reconstructed" as an electron.
The touble is, that this Reconstruction software isnt 100% accurate at reconstructing what ACTUALLY Collided with the detector.
Even though the REconstuction software said that this particle was an electron, it could have in truth been a photon that collided, or 
a positron or some other particle. Pairing the reconstructed partcile with the truth particle that 

The Goal:
------------- 
Is to match each jet with either a quark or a gluon that created it, and then label it, "QIJ" Quark initiated jet or "GIJ" Gluon initated Jet


Prereq:
---------
In order for this script to run, you must setup an athena environment like AthDerivationX.Y
so that root files and their contents, xAOD or otherwise become accessible to you


How to Run this Script:
--------------------------
python analyze_xAOD.py /path/to/root/file/file.root -frac .01


/path/to/root/file/file.root
  This is the root file that you want to run this script on, must be the full path to the scrtpt
frac
  A number between 0 and 1 that will tell the program what percentage of the total events you want to process



'''




import argparse
# import matplotlib.pyplot as plt
# import numpy as np

print("Make sure to have setup an Athena Version")


def histograms():
  import ROOT
  JetScore = ROOT.TH1F("RNNJetScore", "RNNJetScore", 50, 0, 1)
  SigTrans = ROOT.TH1F("RNNJetScore SigTrans","RNNJetScore SigTrans", 50, 0,1)
  
  pt = ROOT.TH1F("pt taus", "pt taus", 100, 15e3, 2e6)
  low_pt = ROOT.TH1F("low pt taus", "low pt taus", 20, 15e3, 100e3)
  
  pt_above_95_flattened = ROOT.TH1F("pt taus when SigTrans>.95", "pt taus when SigTrans>.95", 100, 15e3, 2e6)
  prong_above_95_flattened = ROOT.TH1F("prong # when SigTrans>.95","prong # when SigTrans>.95",5,0,5)
  RNN_score_above_95_flattened = ROOT.TH1F("RNN score when SigTrans>.95","RNN score when SigTrans>.95",100, .95,1)
  
  pt_below_04_flattened = ROOT.TH1F("pt taus when SigTrans<.04", "pt taus when SigTrans<.04", 100, 15e3, 100e3)
  prong_below_04_flattened = ROOT.TH1F("prong # when SigTrans<.04","prong # when SigTrans<.04",5,0,5)
  RNN_score_below_04_flattened = ROOT.TH1F("RNN score when SigTrans<.04","RNN score when SigTrans<.04",50, 0,1)
  
  prong_for_RNNScore_below_0 = ROOT.TH1F("prong # when RNNScore < 0","prong # when RNNScore < 0",5,0,5)







def main(args):

  # Set up ROOT and RootCore:                                                                                      
  import ROOT
  ROOT.gROOT.SetBatch(True)
  #ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )                                                      
  # Initialize the xAOD infrastructure:
  if(not ROOT.xAOD.Init().isSuccess()):
    print("Failed xAOD.Init()")
  
  fileName=args.xAOD
  # Data currently being analyzed is here
  #MULTI_JET_MC16_DATA=/eos/home-j/jcardena/HBSM_analysis/control_region_jets_task/xAODs/MC_xAODs/MultiJet/mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.AOD.e3569_s3126_r10201_r10210/
	#WJET_MC16_DATA=/eos/home-j/jcardena/HBSM_analysis/control_region_jets_task/xAODs/MC_xAODs/WJets/mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.merge.AOD.e5340_s3126_r10201_r10210/


  # Calling all histograms in above function, to make main() more tidy
  histograms()


  
  f = ROOT.TFile.Open(fileName, "READONLY")
  t = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")
  
  print("Number of input events:", t.GetEntries())
  
  
  

  # Determining what fraction of the total events to process
  if args.fraction != 1:
    nevents=int(args.fraction*t.GetEntries()) # Making sure fraction is an integer
    print("Processing {} events\n".format(nevents))
  else:
    nevents = t.GetEntries()
    print("Processing all events: {}".format(t.GetEntries()))


  quark_jets = 0
  gluon_jets = 0

  # Actual data analysis takes place below
  for entry in range(nevents): # Loop through each proton proton bunch crossing "event", stored in the data
  # for entry in range(1):
    print("Entry: {}".format(entry))
    t.GetEntry(entry)         # Loading entry in we are currently on in the loop


    # Initialzing containers to be accessed
    # ------------------------------------------
    # You can view all accessible containers by setting up a release and then doing
    # checkxAOD.py on the .root file that you want to check the containers on.
    jets_container = t.AntiKt4EMPFlowJets
    truth_event_container = t.TruthEvents # Tutorial said that I should access truth particles via Truth Event links or I'll get the pileup particles as well
    tau_jets_container = t.TauJets        # care about the seed jet of the taus, maybe save a flag to the tau that tells it what created it
    truth_particle_container = t.TruthParticles

  # https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf # PdgIDs of particles
  # https://gitlab.cern.ch/atlas/athena/-/blob/master/Event/xAOD/xAODJet/xAODJet/versions/Jet_v1.h # CAllable functions on AntiKt4EMPFlowJets or anything Jet_v1
  # https://gitlab.cern.ch/atlas/athena/-/blob/master/Event/xAOD/xAODJet/xAODJet/versions/JetAccessorMap_v1.h # Can call getattribute for these
  # https://gitlab.cern.ch/atlas/athena/-/blob/master/Event/xAOD/xAODTruth/xAODTruth/versions/TruthParticle_v1.h # functions callable for Truth particles
    for index,jet in enumerate(jets_container):
      print("Jet {} in event {} has pt {}".format(index,entry,jet.pt()))
      # print(jet.PartonTruthLabelID())
      Initiation_particle=jet.getAttribute("PartonTruthLabelID")
      if abs(Initiation_particle) <=6 :
        quark_jets+=1
      if Initiation_particle == 21:
        gluon_jets+=1 
#        print(Initiation_particle)

 #   for true_particle in truth_particle_container:
 #     print("Number of Children for paricle with pdgID {}: {}".format(true_particle.pdgId(), true_particle.nChildren()) )



  print(args.xAOD)
  print("# of Quark Initiated Jets: {}".format(quark_jets))
  print("# of Gluon Initiated Jets: {}".format(gluon_jets))


    # final_state_paricles_with_quark_in_it=[] # Making a list to store all final state particles with quarks in it

    





      # if abs(true_particle.pdgId()) <=6 and true_particle.pdgId() >0 : # every Quark has a PdgId lower than or equal to 6, look here https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf 
      #   # if true_particle.nChildren() == 0: # Requiring that all particles are final state, not having children means they do not decay further, final state.
      #     print("Quark ID: {}, nChildren: {}".format(true_particle.pdgId(),true_particle.nChildren()))
          










    #     particle_p4=true_particle.p4() 
    #   # print("PdgId:{} ".format(truth_particle.pdgId()))


    # for i in truth_event:
    #   # j=i.beamParticles()
    #   bp_1 = i.beamParticle1Link()
    #   print(bp_1.isValid())
    #   print(bp_1.at(0).pdgId())
    #   # truth_particle[bp_1]
    # #   bp_2 = i.beamParticle2Link() #how do I use this link to access the particles in the truth container?
    #   # print(bp_1.pdgId())

    # for true_particle in truth_particle:
    #   if abs(true_particle.pdgId()) <=6:
    #     print("Quark: {}, nChildren: {}".format(true_particle.pdgId(),true_particle.nChildren()))
    #     particle_p4=true_particle.p4() 
    #   # print("PdgId:{} ".format(truth_particle.pdgId()))
 


    # In addition, require the ghost-matched fraction to be at least 50% (GhostTruthAssociationFraction). If the attribute is not available in your derivation, require dR(truth, reco)<0.3.
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/QuarkGluonTagging#MC_Truth_conventions

    # print(truth_particle_container[0])
    # print(jets_container[0])
    # for quark_final_state in quark_final_state:
    # for i in jets_container:
      # if quark final state particle is within .2 dr of jet cone this is a jet match

      # width=i.getAssociatedObjects(ROOT.xAOD.TruthParticle)("GhostTruthParticle")
      # print("GhostTruthParticle Type: ", ROOT.xAOD.JetAttribute.GhostTruthParticle, type(ROOT.xAOD.JetAttribute.GhostTruthParticle))
      # print("GhostTruthAssociationFraction Type: ", ROOT.xAOD.JetAttribute.GhostTruthAssociationFraction, type(ROOT.xAOD.JetAttribute.GhostTruthAssociationFraction))


      # print(ROOT.xAOD.JetAttribute.AssoParticlesID.GhostTruthParticle) # What blake originally posted in the chat
      # width=i.getAssociatedObject(ROOT.xAOD.TruthParticle)(ROOT.xAOD.JetAttribute.GhostTruthParticle)
      # width=i.getAssociatedObject(ROOT.xAOD.TruthParticle)("GhostTruthParticle")
      # width=i.getAttribute("GhostTruthAssociationFraction")
      # width=i.getAttribute("Width")
      # width_2=i.getAttribute("GhostTruthAssociationFraction")
      # print(width)
      # print(width_2)


    # find all the particles that match the jet, a dR of .3 and take the highest energy one

    #   # constituents=i.getConstituents()
    #   constituents=i.constituentLinks()
    #   print(len(constituents))
    #   print("PdgID: {}".format(constituents[0].pdgId()))
      # for jet_part in constituents:
        # print("PdgId:{} ".format(jet_part.pdgId()))



#### NOTES ON WHAT TO DO ####
# delta r cut is passes requirement
# follow the decay chain of the quarks and gluons
# try to determine the what created the jet from the energy of each particle
# loop over taus, loop over truth particles 

# Loop over truth particles and save all final state particles from quarks, .
# Jet ETMiss look at how they distinguish between quarks and gluon jets




# Begin running the main function
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description='Processing a ROOT xAOD')
  parser.add_argument('xAOD', type=str, help='The path to the .root file you want to analyze')
  parser.add_argument('-frac', '--fraction', type=float, default=1, help='The number of events you want to process')
  # parser.add_argument('--sum', dest='accumulate', action='store_const',
  #                     const=sum, default=max,
  #                     help='sum the integers (default: find the max)')
  
  args = parser.parse_args()
  

  main(args)


    # # for i in truth_event:
    # #   # j=i.beamParticles()
    # #   bp_1 = i.beamParticle1Link()
    # #   print(bp_1.isValid())
    # #   print(bp_1.at(0).pdgId())
    # #   # truth_particle[bp_1]
    # # #   bp_2 = i.beamParticle2Link() #how do I use this link to access the particles in the truth container?
    # #   # print(bp_1.pdgId())

    # # for true_particle in truth_particle:
    # #   if abs(true_particle.pdgId()) <=6:
    # #     print("Quark: {}, nChildren: {}".format(true_particle.pdgId(),true_particle.nChildren()))
    # #     particle_p4=true_particle.p4() 
    # #   # print("PdgId:{} ".format(truth_particle.pdgId()))
 


    # # In addition, require the ghost-matched fraction to be at least 50% (GhostTruthAssociationFraction). If the attribute is not available in your derivation, require dR(truth, reco)<0.3.
    # # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/QuarkGluonTagging#MC_Truth_conventions

    # print(truth_particle[0])
    # print(jets[0])
    # # for quark_final_state in quark_final_state:
    # for i in jets:
    #   # if quark final state particle is within .2 dr of jet cone this is a jet match

    #   # width=i.getAssociatedObjects(ROOT.xAOD.TruthParticle)("GhostTruthParticle")
    #   print("GhostTruthParticle Type: ", ROOT.xAOD.JetAttribute.GhostTruthParticle, type(ROOT.xAOD.JetAttribute.GhostTruthParticle))
    #   print("GhostTruthAssociationFraction Type: ", ROOT.xAOD.JetAttribute.GhostTruthAssociationFraction, type(ROOT.xAOD.JetAttribute.GhostTruthAssociationFraction))


    #   # print(ROOT.xAOD.JetAttribute.AssoParticlesID.GhostTruthParticle) # What blake originally posted in the chat
    #   # width=i.getAssociatedObject(ROOT.xAOD.TruthParticle)(ROOT.xAOD.JetAttribute.GhostTruthParticle)
    #   # width=i.getAssociatedObject(ROOT.xAOD.TruthParticle)("GhostTruthParticle")
    #   # width=i.getAttribute("GhostTruthAssociationFraction")
    #   width=i.getAttribute("Width")
    #   width_2=i.getAttribute("GhostTruthAssociationFraction")
    #   print(width)
    #   print(width_2)


    # # find all the particles that match the jet, a dR of .3 and take the highest energy one

    # #   # constituents=i.getConstituents()
    # #   constituents=i.constituentLinks()
    # #   print(len(constituents))
    # #   print("PdgID: {}".format(constituents[0].pdgId()))
    #   # for jet_part in constituents:
    #     # print("PdgId:{} ".format(jet_part.pdgId()))




#EOF 

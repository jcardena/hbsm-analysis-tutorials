#!/usr/bin/env python2.7

# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )


#### MUST SETUP AthDerivation_21.2.6.0 For this to work ####



# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

#fileName = "/cluster/home/bburghgr/truth-hstaustau/daod/DAOD_TRUTH3.test.pool.root"


fileName = "/cluster/home/jcardenas34/new_group_member_introduction/tree.root"
#fileName = "/cluster/home/jcardenas34/qualification_task/ntuple_dir/gamma.root"

f = ROOT.TFile.Open(fileName, "READONLY")
t = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")

print "Number of input events:", t.GetEntries()



dR = ROOT.TH1F("dR tau1 tau2", "dr tau1, tau2", 50, 0, 10)
eta = ROOT.TH1F("Eta","Eta Variable", 100, -1,1)
pt = ROOT.TH1F("pt taus", "taus pt", 10, 0, 2e6)

tracks_1p = ROOT.TH1F("truth1p", "truth1p", 5, 0, 5)
tracks_3p = ROOT.TH1F("truth3p", "truth3p", 5, 0, 5)


for entry in xrange(t.GetEntries()):
  
  t.GetEntry(entry)
  taus = t.TruthTaus
  
  for index in xrange(len(t.TruthTaus)):
    pt.Fill(taus[index].pt())
    


#  if len(taus) != 2:
#    print "Error:", entry, len(taus)
#    continue


 #   ___.DeltaR(____)

#  tau0_p4 = taus[0].p4()
#  tau1_p4 = taus[1].p4()

#  deltaR = tau0_p4.DeltaR(tau1_p4)

#  dR.Fill()
#  eta.Fill(taus[0].eta())
  
  


canvas = ROOT.TCanvas()
pt.Draw("HIST")
canvas.Print("pt-plot.pdf")

#eta.Draw("HIST")
#canvas.Print("test-plot_1.pdf")


print "Finished and done and finished and done"


#!/usr/bin/env python2.7

#### FLATTENING AN NTUPLE ####
#### The purpose of this script is to make a flattened ntuple.
#### It will look into a given root file, and transfer any branches you have selected, that seem important
#### to a new root file, that only contins the information that you want.
#### This process also removes some of the "tree" structure of the original file you are transferring from
####
####
#### You must setup a release  like AthDerivation X.X.X before you run this, in order to get it to work.
####

# For now try to add this branch to your file

#TruthMuons
#TruthElectrons


### This is a sample edit for filezilla


# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

#fileName = "/cluster/home/bburghgr/truth-hstaustau/daod/DAOD_TRUTH3.test.pool.root"
fileName = "/cluster/home/amyrewoldt/DAOD/3lep_Hplus500_slep110_Xn165_Xc120/truth1/DAOD_TRUTH1.aod.pool.root"



f = ROOT.TFile.Open(fileName, "READONLY")             # This loads, and makes the file we have above into an accessible object to use in the code.
t = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")  # There is a tree in the file thats called "Collection Tree", 

print "Number of input events:", t.GetEntries()

outF = ROOT.TFile.Open("tree.root", "RECREATE")       # What the name of your flattened ntuple .root file will be called, "RECREATE" says, if it already exists, delete it and replace it with this new instance of the file
outTree = ROOT.TTree("CollectionTree", "Test TTree")  # What the name of the tree in your root file above will be named

# Create vectors to store tlorentzvectors
# ROOT.vector(ROOT.TLorentzVector)() == std::vector<TLorentzVector>()
taus = ROOT.vector(ROOT.TLorentzVector)()
jets = ROOT.vector(ROOT.TLorentzVector)()
mets = ROOT.vector(ROOT.TLorentzVector)()
muons = ROOT.vector(ROOT.TLorentzVector)()

# Create branches in your new tree that we have defined, theyre basically new arrays for us to store data in
outTree.Branch("taus", taus)
outTree.Branch("jets", jets)
outTree.Branch("mets", mets)
outTree.Branch("muons", muons)

eta = ROOT.TH1F("Eta","Eta Variable", 100, -1,1)
pt = ROOT.TH1F("pt of taus", "taus pt", 50, 0, 600e3)




def search_for_lepton_child_recursion(particle):
  if particle.nChildren() != 0: # Makes sure particle has children 
    for child in range(particle.nChildren()):
      if abs(particle.child(child).pdgId()) == 11:
        print("Child is Electron")
				# electronNegs.push_back(child.p4())
        # ptElectronNegs.Fill(child.p4().Pt())
        # return
      elif abs(particle.child(child).pdgId()) == 13:
        print("Child is Muon") 
				# muonPositives.push_back(child.p4())
        # ptMuonPositives.Fill(child.p4().Pt())
        # return
      else:
        search_for_lepton_child_recursion(particle.child(child))
        
  else:
    print("Has no children left")
		# return




for entry in xrange(t.GetEntries()):
  t.GetEntry(entry)                         # Loads up Entry "entry" in order for us to access all the objects in that event, particle containers in this case

  inTaus = t.TruthTaus                      # Assigns a container, basically an array that exists in the root file called "Truth Taus" that contans all taus for entry "entry" to variable inTaus
  
  inJets = t.AntiKt4TruthDressedWZJets      # Assigning the container in the root file that has all jet objects to the variable called inJets

  inMets = t.MET_Truth                      # Assigning the  container in the root file that has all the Missing transverse energy in formation to inMets
  
  inMuons = t.TruthMuons
  
  inBSM = t.TruthBSM

  inBosons = t.TruthBoson
  
  inBSM = t.TruthBSM

  taus.clear()                              # Clears the information stored in the vector, that came from the previous entry in the loop, to start off with a clean vector for every entry in the for loop were in
  jets.clear()
  mets.clear()
  muons.clear()

  for particle in inBSM:
    print(particle.pdgId())

  # Do cuts and overlap removal here
  for tau in inTaus: 
    taus.push_back(tau.p4())  # loops through the container in the root file that has all the taus and adds each tau vector to the new "taus" vector that will be in your new flattened root file
    pt.Fill(tau.p4().Pt())

  for jet in inJets:
    jets.push_back(jet.p4())

  for met in inMets:
    mp4 = ROOT.TLorentzVector()
    mp4.SetPtEtaPhiM(met.met(), 0, met.phi(), 0) ## eeh a little complicated, look it up on the root documentation, but this really nly needs to be used for the MET vector, and not anywhere else for right now
    mets.push_back(mp4)

  for muon in inMuons:
  	muons.push_back(muon.p4())


  for bsm_particle in inBSM:
    if abs(bsm_particle.pdgId()) == 37:
      search_for_lepton_child_recursion(bsm_particle)
#  	muons = [ [E,px,py,pz],[E,px,py,pz],[],[], ... ]

 # 	muons[1].pt()


  outTree.Fill()
  # print "DEBUG: Event", entry, "taus:", len(taus), "jets:", len(jets), "mets:", len(mets), "muons:", len(muons)


canvas = ROOT.TCanvas()
pt.Draw("HIST")
canvas.Print("pt-plot.pdf")



outF.Write()    # Basically saves the data to your new root file
outF.Close()
print "Finished"
